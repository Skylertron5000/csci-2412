<?php
class Animal {
    // Properties
    public $name;
    public $age;
      // Methods
      function set_name($name) {
        $this->name = $name;
    }
    function set_age($age) {
      $this->age = $age;
  }
    function get_name() {
      return $this->name;
  }
  function get_age() {
    return $this->age;
}
function __construct($name, $age) {
  $this->name = $name;
  $this->age = $age;
}
function __destruct(){
  echo "$this->name is no longer being used, so it's getting destroyed </br>";
}
function echoNameAndAge() {
  echo "$this->name is $this->age years old </br>";
}
}

$spot = new Animal('Spot', 3);
$spot->echoNameAndAge(); 

$fido = new Animal('fido',5);
$fido->echoNameAndAge();
// hey you guys!!!
$fido->echoNameAndAge(); 

$star = new Animal('star', 8);
$star->echoNameAndAge();
// hey you guys!!!
$star->echoNameAndAge(); 

$indigo = new Animal('indigo', 3);
$indigo->echoNameAndAge();
// hey you guys!!!
$indigo->echoNameAndAge(); 

?>

