<?php
class Person {
    public $firstName;
    protected $middleName;
    private $lastName;
    public $fullName;
 
    public function set_firstName($name) {
        $this->firstName = $name;
    }
    public function set_middleName($name) {
        $this->middleName = $name;
    }
    public function set_lastName($name) {
        $this->lastName = $name;
    }
    private function set_fullName() {
        $this->fullName = "$this->firstName $this->middleName $this->lastName";
    }
    function get_firstName() {
        return $this->firstName;
    }
    function get_middleName() {
        return $this->middleName;
    }
    function get_lastName() {
        return $this->lastName;
    }
    public function get_fullName() {
        $this->set_fullName();
        return $this->fullName;
    }
}

$robin = new Person();
$robin->set_firstName("Robinson");
$robin->set_middleName("James");
$robin->set_lastName("Clower");

echo $robin->get_fullName();
}
?>