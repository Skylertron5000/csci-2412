drop database if exists finalProject;

create database finalProject;

use finalProject;

DROP TABLE IF EXISTS Customers;

CREATE TABLE Customers(
    id INT AUTO_INCREMENT PRIMARY KEY,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255)
);

INSERT INTO
    Customers(firstName, lastName, email, password)
VALUES
    (
        "John",
        "Smith",
        "johnsmith@gmail.com",
        "password"
    ),
    (
        "John",
        "Smith",
        "johnsmith@hotmail.com",
        "password"
    ),
    ("Jane", "Doe", "janedoe@gmail.com", "password");

DROP TABLE IF EXISTS Address;

CREATE TABLE Address(
    id INT AUTO_INCREMENT PRIMARY KEY,
    street VARCHAR(255),
    city VARCHAR(255),
    state VARCHAR(2),
    zip INT,
    customerId INT,
    FOREIGN KEY (customerId) REFERENCES Customers (id) ON DELETE CASCADE
);

INSERT INTO
    Address(street, city, customerId)
VALUES
    ("123 Cherry Lane", "Dublin", "OH", 12345, 1),
    ("1 Main Street", "Columbus", "OH", 12345, 2),
    ("12 Loopy Loop", "Dublin", "OH", 12345, 3);

DROP TABLE IF EXISTS Products;

CREATE TABLE Products(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    brand VARCHAR(255),
    description VARCHAR(255),
    price VARCHAR(255)
);

INSERT INTO
    Products(name, brand, description, price)
VALUES
    (
        "Teddy Bear",
        "Diamond Corp",
        "A cute and cuddly teddy bear!",
        "9.99"
    ),
    (
        "Flowers",
        "Linda's Flowers",
        "A super stinky bouquet.",
        "69.99"
    ),
    (
        "Chocolate",
        "Cosby Estate",
        "Special sleepy time chocolates. Mmmmmm.",
        "4.20"
    );

DROP TABLE IF EXISTS Carts;

CREATE TABLE Carts(
    id INT PRIMARY KEY AUTO_INCREMENT,
    quantity INT,
    customerId INT,
    productId INT,
    FOREIGN KEY (customerId) REFERENCES Customers(id) ON DELETE CASCADE,
    FOREIGN KEY (productId) REFERENCES Products(id) ON DELETE CASCADE
);

INSERT INTO
    Carts(quantity, customerId, productId)
VALUES
    (1, 1, 1),
    (1, 1, 2),
    (1, 1, 3),
    (1, 2, 1),
    (1, 3, 2),
    (1, 3, 3);