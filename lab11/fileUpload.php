<?php
$servername = "localhost";
$dbusername = "root";
$dbpassword = "";
$formErr = "";
$uploadDir = "uploads/";
$targetFile = "";
$conn = new PDO("mysql:host=$servername;dbname=lab11", $dbusername, $dbpassword);

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $targetFile = $uploadDir . $_FILES["fileToUpload"]["name"];
    $kbFileSize = $_FILES["fileToUpload"]["size"] / 1000;
    if ($kbFileSize > 500) {
        $formErr = "Your file is too large. Max file size is 500 kb. Yours was $kbFileSize kb";
    }
    if (file_exists($targetFile)) {
        $formErr = "File already exists";
    }
    // if (!empty($targetFile) && empty($formErr) && move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetFile)) {
    //     echo "The file " . htmlspecialchars(basename($_FILES["fileToUpload"]["name"])) . " has been uploaded.";
    // } else {
    //     echo "Sorry, there was an error uploading your file.";
    // }
    try {
        $conn = new PDO("mysql:host=$servername;dbname=lab11", $dbusername, $dbpassword);

        if (!empty($targetFile) && empty($formErr)) {
            $imageName = htmlspecialchars($_FILES["fileToUpload"]["name"]);
            $imageData = file_get_contents($_FILES['fileToUpload']['tmp_name']);
            $altTag = htmlspecialchars($_POST['altTag']);
            uploadFile($conn, $imageName, $imageData, $altTag);
        }
    } catch (PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }
}

function uploadFile($conn, $imageName, $imageData, $altTag)
{
    $insert = "INSERT INTO images (imageName, image, altTag)
    VALUES (:imageName, :image, :altTag)";
    $stmt = $conn->prepare($insert);
    $stmt->bindParam(':imageName', $imageName);
    $stmt->bindParam(':image', $imageData);
    $stmt->bindParam(':altTag', $altTag);
    $stmt->execute();
}

function displayImage($conn)
{
    $select = "SELECT image, altTag FROM images";
    $stmt = $conn->prepare($select);
    $stmt->execute();

    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach ($stmt->fetchAll() as $listRow) {
        $image = $listRow['image'];
        $altTag = $listRow['altTag'];
        echo "
                <div class='col-6' alt='".$altTag."'>
                    <img src='data:image/jpeg;base64," . base64_encode($image) . "' />
                </div>
                ";
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
</head>

<body>

    <style>
        .error {
            color: #FF0000;
        }

        img {
            max-width: 100%;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-8 offset-lg-2">
                <form action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="fileToUpload">Select image to upload:</label>
                        <input type="file" name="fileToUpload" id="fileToUpload" required>
                        <label for="altTag">Alternate text for the image:</label>
                        <input type="text" name="altTag" id="altTag" required>
                        <span class="error">* <?php echo $formErr; ?></span><br>
                    </div>
                    <input type="submit" value="Upload Image" name="submit">
                </form>
            </div>
        </div>
        <div class="row">
            <?php displayImage($conn); ?>
        </div>
    </div>
</body>

</html>