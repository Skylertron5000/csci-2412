<html>
    <?php
        include("fuelHeader.php");
    ?>
<body>
<div class="container">
    <h1>Fuel Calculator</h1>
    <?php
    $miles = "";
    $milesErr = "";
    $mpg = "";
    $mpgErr = "";
    $gas = "";
    $groceries = "";
    $groceriesErr = "";
    $cost = "";

      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $miles = clean_input($_POST["miles"]);
        if (!is_numeric($miles)) {
            $milesErr = "Input must be a number";
            $miles = ""; // clear invalid result for $favoriteNumber
          }
        
        $mpg = clean_input($_POST["mpg"]);
        if (!is_numeric($mpg)) {
            $mpgErr = "Input must be a number";
            $mpg = "";
        }

        $gas = $_POST["gas"];

        $groceries = clean_input($_POST["groceries"]);
        if (!is_numeric($groceries)) {
            $groceriesErr = "Input must be a number";
            $groceries = "";
        }

        if ($groceries == "") {
            $groceries = 0;
        }

        $cost = number_format((float)($miles / $mpg * ($gas - floor($groceries / 100) * 0.10)), 2, '.', '');

      }
   
      function clean_input($data) {
        $data = trim($data); // removes whitespace
        $data = stripslashes($data); // strips slashes
        $data = htmlspecialchars($data); // replaces html chars
        return $data;
      }
  ?>
  <style>
    .error {color: #FF0000;}
  </style>
  <p><span class="error">* required field</span></p>
<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<label for="miles">Average miles driven:</label>
<input type="text" name="miles" id="miles" required>
<span class="error">* <?php echo $milesErr?></span><br>

<label for="mpg">MPG of your car:</label>
<input type="text" name="mpg" id="mpg" required>
<span class="error">* <?php echo $mpgErr?></span><br>

<p>Gas you use in your car:</p>
<input type="radio" id="87" name="gas" value="1.89" required>
<label for="87">87 Octone - $1.89</label><br>
<input type="radio" id="89" name="gas" value="1.99">
<label for="89">89 Octone - $1.99</label><br>
<input type="radio" id="92" name="gas" value="2.09">
<label for="92">92 Octone - $2.09</label><br>

<label for="groceries">Amount spent on groceries:</label>
<input type="text" name="groceries" id="groceries"><br>

<input type="submit" value="Submit">  
</form>
<p>Your weekly fuel cost is: <?php echo "$ $cost"; ?> </p>
</div>
</body>
</html>
