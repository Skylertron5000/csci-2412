use lab6;
select * from courseinfo;
INSERT INTO movies(title, releaseDate)
VALUES
("Jaws" , "2021-06-10"); 
SELECT * FROM movies;
DROP TABLE movies; 
CREATE TABLE movies(
 primaryKey INT AUTO_INCREMENT PRIMARY KEY,
 title VARCHAR(255),
 director VARCHAR(255),
 releaseDate DATE
);
 
INSERT INTO movies(title, director, releaseDate)
VALUES
("Lawrence of Arabia", "David Lean", "1969-12-16"); 
 
INSERT INTO movies(title, director, releaseDate)
VALUES
("Troll 2", "Claudio Fragasso", "1990-10-12"); 
