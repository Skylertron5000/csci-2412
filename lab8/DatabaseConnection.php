<?php
$servername = "localhost";
$username = "root";
$password = "";
 
try {
  $conn = new PDO("mysql:host=$servername;dbname=lab8", $username, $password);
  selectStarFromTable($conn, "Teams");
  selectStarFromTable($conn, "Players");
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}

function selectStarFromTable($conn, $table) {
  $query = "SELECT * FROM $table";
  $stmt = $conn->prepare($query);
  $stmt->execute();
  $stmt->setFetchMode(PDO::FETCH_ASSOC);
  foreach($stmt->fetchAll() as $allData) {
    echo "<br>";
    foreach($allData as $data) {
      echo "$data <br>";
    }
  }
}
