DROP DATABASE IF EXISTS lab8;
CREATE DATABASE lab8;
USE lab8;
 
DROP TABLE IF EXISTS Teams;
CREATE TABLE Teams(
    teamId INT AUTO_INCREMENT PRIMARY KEY,
    teamName VARCHAR(255),
    homeCity VARCHAR(255),
    league VARCHAR(255)
);
 
INSERT INTO Teams(teamName, homeCity, league)
VALUES
("Buckeyes", "Columbus", "NCAAF"),
("Blue Jackets", "Columbus", "NHL"),
("Browns", "Cleveland", "NFL"),
("Reds", "Cincinnati", "MLB");
   
DROP TABLE IF EXISTS Players;
CREATE TABLE Players(
    playerId INT AUTO_INCREMENT PRIMARY KEY,
    jerseyNumber INT,
    playerName VARCHAR(255),
    team INT,
    FOREIGN KEY (team) REFERENCES teams(teamId) ON DELETE CASCADE
);
   
INSERT INTO Players(jerseyNumber, playerName, team)
VALUES
("7", "CJ Stroud", "1"),
("2", "Chris Olave", "1"),
("28", "Oliver Bjorkstrand", "2"),
("13", "Odell Beckham Jr.", "3"),
("6", "Lebron James", NULL),
("8", "Cal Ripken Jr.", NULL);

SELECT * FROM Players AS p LEFT JOIN Teams AS t ON p.team = t.teamId WHERE t.league = "NFL";
