<?php
class CashRegister {
    protected $amountInRegister;
    public function __construct($amountInRegister) {
        $this->amountInRegister = $amountInRegister;
    }
    public function __destruct(){
    }
    public function getAmountInRegister() {
        return $this->amountInRegister;
    }
    public function setAmountInRegister($amount) {
        $this->amountInRegister = $amount;
    }
    public function addMoney($amount) {
        $this->amountInRegister = $this->amountInRegister + $amount;
    }
    public function removeMoney($amount) {
        if ($amount <= $this->amountInRegister) {
            $this->amountInRegister = $this->amountInRegister - $amount;
        } else {
            echo "You can't remove that much money!</br>";
        }
    }
}
?>
