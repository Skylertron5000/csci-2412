<?php
include("CashRegister.php");
class PiggyBank extends CashRegister {
    public function removeMoney($amount) {
        echo "You can't remove money from a piggy bank, you have to smash it!</br>";
    }
    public function breakBank() {
        echo "We smashed the Piggy Bank and removed $$this->amountInRegister from inside!</br>";
        $this->amountInRegister = 0;
    }
}
?>