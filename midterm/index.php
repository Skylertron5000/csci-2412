<html>

<body>
    <?php
    include('PiggyBank.php');
    echo "Create both with $20</br></br>";
    $piggyBank = new PiggyBank(20);
    $cashRegister = new CashRegister(20);

    echo "piggy bank has $" . $piggyBank->getAmountInRegister() . "</br>";
    echo "cash register has $" . $cashRegister->getAmountInRegister() . "</br>";

    echo "</br>Add $10 to both</br></br>";
    $piggyBank->addMoney(10);
    $cashRegister->addMoney(10);

    echo "piggy bank has $" . $piggyBank->getAmountInRegister() . "</br>";
    echo "cash register has $" . $cashRegister->getAmountInRegister() . "</br>";

    echo "</br>Remove $5 from both</br></br>";
    $piggyBank->removeMoney(5);
    $cashRegister->removeMoney(5);

    echo "piggy bank has $" . $piggyBank->getAmountInRegister() . "</br>";
    echo "cash register has $" . $cashRegister->getAmountInRegister() . "</br>";

    echo "</br>Remove $70 from both</br></br>";
    $piggyBank->removeMoney(70);
    $cashRegister->removeMoney(70);

    echo "piggy bank has $" . $piggyBank->getAmountInRegister() . "</br>";
    echo "cash register has $" . $cashRegister->getAmountInRegister() . "</br>";

    echo "</br>Break piggy bank</br></br>";
    $piggyBank->breakBank();

    $countErr = "";

    function clean_input($data)
    {
        $data = trim($data); // removes whitespace
        $data = stripslashes($data); // strips slashes
        $data = htmlspecialchars($data); // replaces html chars
        return $data;
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $count = clean_input($_POST["count"]);
        if (!is_numeric($count)) {
            $countErr = "The number of times to repeat must be a number!";
            $count = "";
        } else {
            echo "</br>";
            for ($i = 0; $i < $count; $i++) {
                echo $_POST["tongueTwister"] . "</br>";
            }
        }
    }
    ?>
    </br>
    </br>
    <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div>
            <label for="tongueTwister">Tounge Twister</label>
            <select name="tongueTwister" id="tongueTwister" required>
                <option value="Peter Piper picked a peck of pickled peppers">Peter Piper picked a peck of pickled peppers</option>
                <option value="She sells seashells by the seashore">She sells seashells by the seashore</option>
                <option value="How much wood would a woodchuck chuck if a woodchuck could chuck wood?">How much wood would a woodchuck chuck if a woodchuck could chuck wood?</option>
            </select>
        </div>
        <div>
            <label for="count">Number of Times to repeat</label>
            <input type="text" name="count" id="count" required>
            <span class="error">* <?php echo $countErr; ?></span><br>
        </div>
        <div>
            <input type="submit" value="Submit">
        </div>
    </form>
</body>

</html>