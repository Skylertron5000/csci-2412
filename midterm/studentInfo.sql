drop database if exists midterm;
create database midterm;
use midterm;
 
DROP TABLE IF EXISTS User;
CREATE TABLE User(
    id INT AUTO_INCREMENT PRIMARY KEY,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    isStudent BOOLEAN
);
 
INSERT INTO User(firstName, lastName, isStudent)
VALUES
("Jeane", "Duffey", TRUE),
("John", "Smith", FALSE);
 
SELECT * FROM User WHERE isStudent is TRUE;
